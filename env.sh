#!/bin/sh

export GIT_SSL_NO_VERIFY=1

#-----------------------------------------------------------------------------
# Configuration

# Defines the user name for access to GIT repositories. 
USERNAME=alarcher

# Defines the MPI implementation used.
# Values: mpich, openmpi
MPI=mpich

# Defines the compiler used.
# Values: gcc, llvm
COMPILER=gcc

# Defines the platform used.
# Values: i386, amd64
ARCH=amd64

# Defines the root directory of the install, use should have write permission.
ROOT=/opt/ctl

# Defines the root directory of the ports
KTHPORTSDIR=/local/larcher/ctl/kthports

################################################################################
# Do not edit below this line !                                                #
################################################################################

#-----------------------------------------------------------------------------
# Unset compiler and linker flags.
#unset CC
#unset CFLAGS
#unset CXX
#unset CXXFLAGS
#unset FC
#unset FCFLAGS
#unset LDFLAGS
#unset LD_LIBRARY_PATH

#-----------------------------------------------------------------------------
# Export environment variables.
VARS_PREFIX="KTHPORTS"
VARS="USERNAME MPI COMPILER ARCH ROOT"

for var in ${VARS}
do
    echo ${VARS_PREFIX}_${var}=${!var}
    if [ "X" == "X${var}" ]
    then
	    echo "Variable ${var} not set in the configuration."
        exit 1
    else
        export ${VARS_PREFIX}_${var}=${!var}
    fi
done

#-----------------------------------------------------------------------------
# Root installation directory and ports directory

export KTHOPTROOT=${ROOT}/${MPI}/${COMPILER}/${ARCH}
export KTHPORTSDIR

#-----------------------------------------------------------------------------
# Python

PYTHONVERSION=`python -c 'import sys; print sys.version[:3]'`
PYTHONPATH=${KTHPORTSDIR}/dolfin-hpc/build/dolfin-hpc/site-packages:${PYTHONPATH}
PYTHONPATH=${KTHOPTROOT}/lib/python${PYTHONVERSION}/site-packages:${PYTHONPATH}

export PYTHONPATH

#-----------------------------------------------------------------------------
# Pkgconfig

PKG_CONFIG_PATH=${KTHOPTROOT}/lib/pkgconfig:${PKG_CONFIG_PATH}

export PKG_CONFIG_PATH

#-----------------------------------------------------------------------------
# System specific compiler flags

ARCHFILE=${KTHPORTSDIR}/arch/`uname -s`-${MPI}-${COMPILER}-${ARCH}.sh

if test -e ${ARCHFILE}
then
	source ${ARCHFILE}
else
	echo "Architecture specific file not found: ${ARCHFILE}"
fi 

#-----------------------------------------------------------------------------
# System specific compiler flags

export LD_LIBRARY_PATH=${KTHOPTROOT}/lib:${LD_LIBRARY_PATH}
export PATH=${KTHOPTROOT}/bin:${PATH}




